##Prerequisites

Install the latest version of Windows 10 (Version 1903+, Build 18362+)

Install Windows Subsystem for Linux (WSL), including a Linux distribution (like Ubuntu) and make sure it is running in WSL 2 mode.
 
 You can check this by opening PowerShell and entering: wsl -l -v

Install Node.js on WSL 2: These instructions use Node Version Manager (nvm) for installation, you will need a recent version of NodeJS to run create-react-app, as well as a recent version of Node Package Manager (npm). For exact version requirements, see the Create React App website.


##Create your React app

To install the full React toolchain on WSL, we recommend using create-react-app:

Open a terminal(Windows Command Prompt or PowerShell).

Create a new project folder: mkdir ReactProjects and enter that directory: cd ReactProjects.

Install React using create-react-app, a tool that installs all of the dependencies to build and run a full React.js application:

npx create-react-app my-app

This will first ask for your permission to temporarily install create-react-app and it's associated packages. Once completed, change directories into your new app ("my-app" or whatever you've chosen to call it): cd my-app.

##Start your new React app:


npm start
This command will start up the Node.js server and launch a new browser window displaying your app. You can use Ctrl + c to stop running the React app in your command line.

When you're ready to deploy your web app to production, running npm run build will create a build of your app in the "build" folder.

##Reference:

https://docs.microsoft.com/en-us/windows/dev-environment/javascript/react-on-windows#create-your-react-app
